import app from './app';
import * as dotenv from 'dotenv';
import { logger } from './config/logger.config';

dotenv.config();

logger.info("Upstreet Challenge with Typescript and NodeJS")

app.init();
