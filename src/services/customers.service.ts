import { logger } from '../config/logger.config';
import axios, {AxiosInstance} from 'axios';
import https from 'https';

let axiosRequest: AxiosInstance = axios.create({
    baseURL: process.env.API_ENDPOINT,
    httpsAgent: new https.Agent({keepAlive: true}),
    headers: {
              'token': process.env.API_KEY,
              'Content-Type': 'application/json'
          }
});


export async function checkKYC(params: any) {    
    let response= await axiosRequest.post('driverlicence',params)
     logger.info(JSON.stringify(response.data))
     return response.data
}


export async function execRequest (request: any) {
  const data = await checkKYC(request);
  let response

  switch (data.verificationResultCode) {
    case "Y":
      response = {'kycResult': true}
      break;
    case "N":
      response = {'kycResult': false}
      break;
    case "D":
      response = {
        'code': 'D',
        'message': 'Document Error'
      }
      break;
    case "S":
      response = {
        'code': 'S',
        'message': 'Server Error'
      }
      break;
  }

  return response
}
