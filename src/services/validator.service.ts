import { logger } from '../config/logger.config';
import { CustomerRequest } from "../type/validations.type";

export function validateCustomerRequest(customerRequest: CustomerRequest) {

    let response: Array<any> = [];
    let patternDate = new RegExp(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/);

    if(!patternDate.test(customerRequest.birthDate)) {
        logger.error("Invalid date of birth format (YYYY-MM-DD)")
        response.push("Invalid date of birth (YYYY-MM-DD)")
    }

    if(customerRequest.expiryDate)
        if(!patternDate.test(customerRequest.expiryDate)) {
            response.push("Invalid expiration date (YYYY-MM-DD)")
            logger.error("Invalid expiration date format (YYYY-MM-DD)")
        }

    if (customerRequest.givenName === null) {
        response.push("givenName field is required or mandatory")
        logger.error("givenName field is required or mandatory")
    }

    if ((customerRequest.givenName.length > 100) || (customerRequest.familyName.length > 100)) {
        response.push("Some Name field must exceed 100 characters")
        logger.error("Some Name field must exceed 100 characters")
    }
    
    if(customerRequest.middleName) validateName(customerRequest.middleName, response)

    
    // logger.log(response)
    return response;


}

function validateName(name: string, response: any) {
    if (name.length > 100) {
        response.push("Some Name field must exceed 100 characters");
        logger.error(`Some ${name} field must exceed 100 characters`)
        return true
    }
    return false
}

function validateDate(date: string, response: any) {
    let patternDate = new RegExp(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/);

    if(!patternDate.test(date)) {
        response["birthDate"] = "Invalid date of birth (YYYY-MM-DD)"
        logger.error("Invalid date of birth format (YYYY-MM-DD)")
        return true
    }
    return false
}