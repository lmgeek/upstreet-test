// const axios = require('axios');
import "dotenv/config.js";
import { logger } from './config/logger.config';
import { execRequest } from './services/customers.service';
import { CustomerRequest } from "./type/validations.type";
import { validateCustomerRequest } from './services/validator.service';


class App {  
    
    customerRequest: CustomerRequest = {
        birthDate: "1985-02-08",
        givenName: "James",
        middleName: "Robert",
        familyName: "Smith",
        licenceNumber: "94977000",
        stateOfIssue: "NSW",
        expiryDate: "2020-01-01"
    }
    
    public init() {

        let promise = validateCustomerRequest(this.customerRequest);
        logger.info(`validateCustomerRequest: ${promise}`)
        let customerResponse = execRequest(this.customerRequest)
        
        
        
        customerResponse.then((data: any) => {
            if(data.code){
                logger.error(JSON.stringify(data))
            }else{
                logger.info(JSON.stringify(data));
            }
        }).catch((err: { message: any; }) => {
            logger.error(err.message)
        })
        
    }
    
    
    
}

export default new App();