import winston from 'winston';

export const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.errors({stack: true}),
        winston.format.timestamp(),
        winston.format.json(),
    ),
    defaultMeta: {service: 'user-service'},
    transports: [
        new winston.transports.Console({
             format: winston.format.combine(
                 winston.format.colorize(),
                 winston.format.printf(info => {
                     const log = `[${info.timestamp}] ${info.level}: ${info.message}`;
                     return info.stack ? `${log}\n${info.stack}` : log
                 }),
             ),
            level: 'info'
        })
    ]
})

