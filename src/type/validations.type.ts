export interface CustomerRequest {
    birthDate: string,
    givenName: string,
    middleName?: string,
    familyName: string,
    licenceNumber: string,
    stateOfIssue: StateOfIssue,
    expiryDate?: string
}


export type StateOfIssue =  'NSW' | 'QLD' | 'SA' | 'TAS' | 'VIC' | 'WA' | 'ACT' | 'NT'; 