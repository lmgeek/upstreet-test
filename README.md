# UpStreet Test
> Test for Upstreet

## Technologies

* 	-  [NodeJS](https://nodejs.org/en/)
* 	-  [Typescript](https://www.typescriptlang.org/) 


## Installation 🚀
```sh
git clone https://gitlab.com/lmgeek/upstreet-test
cd upstreet-test
npm i
```

## Run
```
# production
$ npm run start 

# development
$ npm start:dev
```



## Meta


_My name is Luis Marin i live on Argentina but born in Venezuela._
